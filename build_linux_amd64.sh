#!/bin/bash
targetDir="build"
appName="go-sample"
GOOS=linux GOPROXY=https://goproxy.io GOARCH=amd64 go build -o "${targetDir}/${appName}" cmd/${appName}/main.go
