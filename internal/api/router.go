package api

import (
	"gitee.com/thingple/go-web-sample/internal/api/deviceApi"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/middleware/recover"
)

func Route(app *iris.Application) {
	app.Use(recover.New())
	route(app.Party("/api"))
}

func route(root iris.Party) {
	device(root.Party("/dev"))
}

func device(p iris.Party) {
	p.Get("/", deviceApi.List) // 用户注册
}