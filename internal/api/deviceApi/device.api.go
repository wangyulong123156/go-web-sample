package deviceApi

import (
	"gitee.com/thingple/go-web-sample/internal/common"
	"gitee.com/thingple/go-web-sample/internal/db/repo"
	"gitee.com/thingple/go-web-sample/internal/db/service"
	"github.com/kataras/iris/v12"
	"github.com/lishimeng/app-starter"
	"github.com/lishimeng/go-log"
)

type Device struct {
	DeviceID                  int    `json:"deviceId,omitempty"`
	DeviceName                string `json:"deviceName,omitempty"`
	DeviceCode                string `json:"deviceCode,omitempty"`
	AppId                     string `json:"appId,omitempty"`
	DeviceCategoryId          int    `json:"deviceCategoryId"`
	DeviceCategory            string `json:"deviceCategory,omitempty"`
	DeviceCategoryDescription string `json:"deviceCategoryDescription"`
	CreateTime                string `json:"ctime,omitempty"`
}

type RegisterResp struct {
	Uid int `json:"uid,omitempty"`
	app.Response
}

func List(ctx iris.Context) {
	var err error
	var resp app.PagerResponse

	var pageSize = ctx.URLParamIntDefault("s", repo.DefaultPageSize)
	var pageNum = ctx.URLParamIntDefault("n", repo.DefaultPageNo)
	var deviceName = ctx.URLParamDefault("name", "")
	var deviceCode = ctx.URLParamDefault("code", "")

	page := app.Pager{
		PageSize: pageSize,
		PageNum:  pageNum,
	}

	page, devices, err := service.GetDevices(page, deviceName, deviceCode)
	if err != nil {
		log.Info("Get deviceApi List err")
		log.Info(err)
		resp.Code = common.RespCodeError
		resp.Message = common.RespMsgError
		common.ResponseJSON(ctx, resp)
		return
	}

	for _, d := range devices {
		page.Data = append(page.Data, Device{
			DeviceID:         d.DeviceID,
			DeviceName:       d.DeviceName,
			DeviceCode:       d.DeviceCode,
			AppId:            d.AppId,
			DeviceCategoryId: d.DeviceCategoryId,
			CreateTime:       common.FormatTime(d.CreateTime),
		})
	}

	resp.Pager = page
	resp.Code = common.RespCodeSuccess
	common.ResponseJSON(ctx, resp)
}
