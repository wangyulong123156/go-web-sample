package etc

var Config Configuration

type Configuration struct {
	Name    string `toml:"name"`
	Version string `toml:"version"`
	Db      db     `toml:"db"`
	Web     web    `toml:"web"`
	Lora    lora   `toml:"lora"`
	Mqtt    mqtt   `toml:"mqtt"`
}

type db struct {
	Host     string `toml:"host"`
	Database string `toml:"database"`
	Port     int    `toml:"port"`
	User     string `toml:"user"`
	Password string `toml:"password"`
	Ssl      string `toml:"ssl"`
}

type web struct {
	Listen string `toml:"listen"`
}

type lora struct {
	Host     string `toml:"host"`
	Username string `toml:"username"`
	Password string `toml:"password"`
}

type mqtt struct {
	Host            string `toml:"Host"`
	UserName        string `toml:"UserName"`
	Password        string `toml:"Password"`
	ClientId        string `toml:"ClientId"`
	TopicOfTaskUp   string `toml:"TopicOfTaskUp"`
	TopicOfTaskDown string `toml:"TopicOfTaskDown"`
}
