package internal

import (
	"context"
)

func Setup(ctx context.Context) (err error) {

	modules := []func(context.Context) error{func(ctx context.Context) error {
		return nil
	}}

	for _, m := range modules {
		err = m(ctx)
		if err != nil {
			break
		}
	}
	return
}
