package service

import (
	"gitee.com/thingple/go-web-sample/internal/db/model"
	"gitee.com/thingple/go-web-sample/internal/db/repo"
	"github.com/astaxie/beego/orm"
	"github.com/lishimeng/app-starter"
)

func GetDevices(page app.Pager, name, code string) (p app.Pager, devices []model.Device, err error) {

	cond := orm.NewCondition()
	if len(name) > 0 {
		cond = cond.And("DeviceName", name)
	}
	if len(code) > 0 {
		cond = cond.And("DeviceCode", code)
	}
	p, devices, err = GetDeviceList(page, cond)
	return
}

func GetDeviceList(page app.Pager, condition *orm.Condition) (p app.Pager, devices []model.Device, err error) {

	//sum
	var qs = app.GetOrm().Context.QueryTable(new(model.Device)).SetCond(condition)
	sum, err := qs.Count()
	if err != nil {
		return
	}
	page.TotalPage = repo.CalcTotalPage(page, sum)

	//result_set
	_, err = qs.OrderBy("DeviceId").Offset(repo.CalcPageOffset(page)).Limit(page.PageSize).SetCond(condition).All(&devices)
	if err != nil {
		return
	}
	p = page
	return
}

func GetDevice(d *model.Device) error {
	ctx := app.GetOrm()
	err := repo.GetDevice(*ctx, d)
	return err
}

func GetDeviceByDeviceCode(d *model.Device) error {
	ctx := app.GetOrm()
	err := repo.GetDeviceByDeviceCode(*ctx, d)
	return err
}
