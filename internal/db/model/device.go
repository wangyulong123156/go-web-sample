package model

type Device struct {
	DeviceID         int    `orm:"pk;auto;column(device_id)"`
	DeviceName       string `orm:"column(device_name);null"`
	DeviceCode       string `orm:"column(device_code);null"`
	AppId            string `orm:"column(app_id);null"`
	DeviceCategoryId int    `orm:"column(device_category_id);null"`
	TableInfo
}
