package repo

import (
	"gitee.com/thingple/go-web-sample/internal/db/model"
	persistence "github.com/lishimeng/go-orm"
)

func AddDevice(ctx persistence.OrmContext, device *model.Device) error {
	_, err := ctx.Context.Insert(device)
	return err
}

func GetDevice(ctx persistence.OrmContext, device *model.Device) error {
	err := ctx.Context.Read(device)
	return err
}

func UpdateDevice(ctx persistence.OrmContext, device *model.Device) error {
	_, err := ctx.Context.Update(device)
	return err
}

func GetDeviceByDeviceCode(ctx persistence.OrmContext, device *model.Device) error {
	err := ctx.Context.Read(device, "DeviceCode")
	return err
}

func DeleteDevice(ctx persistence.OrmContext, device *model.Device) error {
	_, err := ctx.Context.Delete(device)
	return err
}
