module gitee.com/thingple/go-web-sample

go 1.16

require (
	github.com/astaxie/beego v1.12.1
	github.com/kataras/iris/v12 v12.1.8
	github.com/lib/pq v1.0.0
	github.com/lishimeng/app-starter v1.4.0
	github.com/lishimeng/go-log v1.0.0
	github.com/lishimeng/go-orm v1.0.4
)
